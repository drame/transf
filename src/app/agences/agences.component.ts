import { Component, OnInit,ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { AgenceService } from '../services/agence.service';
@Component({
  selector: 'app-agences',
  templateUrl: './agences.component.html',
  styleUrls: ['./agences.component.css']
})
export class AgencesComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  options={
    types: [],
    componentRestrictions: { country: 'MR' }
    }
    @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  constructor(private formBuilder: FormBuilder,private agenceService:AgenceService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      firstname: ['', Validators.required],
      cin: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required]]
  });
  }
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      this.agenceService.add(this.registerForm.value).subscribe(resp=>{
        console.log(resp);
      })
  }
  public handleAddressChange(address) {
    // Do some stuff
}
}

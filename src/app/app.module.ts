import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {HttpClientModule} from '@angular/common/http'; 
import { JwtModule } from '@auth0/angular-jwt';
import { AuthGuard } from './auth.guard';
import {AuthService} from './services/auth.service';
import {AgenceService} from './services/agence.service';
import { HomeComponent } from './home/home.component';
import { TransfertComponent } from './transfert/transfert.component';
import { AgencesComponent } from './agences/agences.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MapComponent } from './map/map.component'
export function tokenGetter() {
  return localStorage.getItem('access_token');
}
const routes:Routes=[{
  component:LoginComponent,
  path:'login',
  canActivate:[AuthGuard]
},
{
  component:HomeComponent,
  path:'home',
  canActivate:[AuthGuard],
  children:[
    { path: '', redirectTo: 'transfert', pathMatch: 'full' },
    { path: 'transfert', component: TransfertComponent },
    { path: 'agences', component: AgencesComponent },
  ]
},
{ path: '', redirectTo: 'home', pathMatch: 'full' }]
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    TransfertComponent,
    AgencesComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    GooglePlaceModule,
    RouterModule.forRoot(routes),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:4000'],
        blacklistedRoutes: ['localhost:8080/login']
      }
    })
  ],
  providers: [AuthService,AgenceService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

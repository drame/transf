import { Injectable } from '@angular/core';
import { CanActivate,Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (localStorage.getItem('access_token')) {
        if(state.url=="/login"){
          this.router.navigate(['/home']);
          return false;
        }
        return true;
      }
      else{
        if(state.url=="/login"){
          return true;
        }
        else{

          this.router.navigate(['/login']);
          return false;
        }
      }
  
     
  }
}

import { Component, OnInit } from '@angular/core';
import {AuthService} from './../services/auth.service';
import { Route, Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
public loginError={
  error:false,
  message:""
}
  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit() {
  }
login(f:any){
  this.loginError.error=false;
  console.log(f)
  this.authService.login(f.username,f.password).subscribe(result=>{
    if(result.accessToken){
      localStorage.setItem('access_token', result.accessToken);
      this.router.navigate(['/home']);
    }
  },resp=>{
    console.log(resp)
    this.loginError.error=true;
    this.loginError.message=resp.error.error;
  })
}
}

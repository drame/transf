import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AgenceService {
  private host="http://localhost:8080";
  constructor(private http: HttpClient) { }

  add(agence) {
    return this.http.post<{agence: object}>(this.host+'/agence', agence);
  }
}
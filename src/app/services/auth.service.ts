import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService {
  private host="http://localhost:8080";
  constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    return this.http.post<{accessToken: string}>(this.host+'/login', {username: username, password: password});
  }

  logout() {
    localStorage.removeItem('access_token');
  }

  public get loggedIn(): boolean {
    return (localStorage.getItem('access_token') !== null);
  }
}